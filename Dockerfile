FROM node:16-alpine

ARG NODE_ENV=development

WORKDIR /
RUN yarn create strapi-app app --quickstart --no-run

COPY ./docker-entrypoint.sh /app/docker-entrypoint.sh

WORKDIR /app
RUN yarn config set network-timeout 600000 -g
RUN yarn add strapi-media-library
RUN yarn install --frozen-lockfile

# Copy Strapi source files to temporary folder
# Restore from here if volume overrides it with empty directory
RUN mkdir /backup
RUN cp -r /app/src /backup/src

EXPOSE 1337
ENTRYPOINT [ "/app/docker-entrypoint.sh" ]
