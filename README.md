# 🔥 MTT Daloskönyv Strapi

## Telepítés

A repó tartalmaz egy [docker-compose.yaml](./docker-compose.yaml) fájlt. Egy konténer indul, a CMS adatbázisként egy SQLite fájlt használ. A konténer a következő paranccsal indítható:

```sh
docker compose up -d
```

A konténerben futó alkalmazás az `1337`-es porton lesz elérhető. Ezt kell valamilyen módon elérhetővé tenni a web felé.
<br>
<br>

## Konfigurálás

A Strapi bizonyos mappáit mountolni kell a fájlrendszerre, hogy konténer failure esetén ne vesszenek el a felvitt adatok. A mount elérési utak a [docker-compose.yaml](./docker-compose.yaml) fájlban konfigurálhatóak:

- `/app/src/:` Generált JavaScript fájlok
- `/app/data/:` SQLite bináris állomány
- `/app/config/:` Alkalmazás konfigurációs fájlok
- `/app/public/uploads:` Feltöltött fájlok

A biztonságos futáshoz a következő változókat kell beállítani a compose-t futtató környezetben:

- `STRAPI_ADMIN_JWT_SECRET:` Admin belépést titkosító kulcs
- `STRAPI_ADMIN_TOKEN_SALT:` API token generáláshoz használt kulcs

<br>

_Jó munkát, jó dalolást!_ 🔥👋
