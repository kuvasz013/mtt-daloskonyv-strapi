module.exports = ({ env }) => ({
  auth: {
    secret: env("ADMIN_JWT_SECRET", "*:3Px7uTc:Up4RHiosFWUM@uVx}-*W"),
  },
  apiToken: {
    salt: env("API_TOKEN_SALT", "p=*:F,~n7fbqXn7P=FyvVgr^Mb#~D*"),
  },
});
