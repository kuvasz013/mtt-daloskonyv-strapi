module.exports = ({ env }) => ({
  connection: {
    client: "sqlite",
    connection: {
      filename: env("DATABASE_FILENAME", "data/sqlite.db"),
    },
    useNullAsDefault: true,
    debug: false,
  },
});
