#!/bin/sh

echo "Checking if Strapi files exist in '/app/src/api'..."

# If src folder is empty, write backup files
if ([ "$(ls -A /app/src/api)" ])
then
  echo "Using mounted files in: '/app/src/api'"
else
  echo "Unable to start Strapi! Cannot access '/app/src/api', or directory is empty."
  echo "Attempting to restore backup..."
  cp -r '/backup/src' '/app/'
fi

yarn --cwd=/app develop